﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExperisNoroffTask5 {
    class Person {
        string firstName;
        string lastName;

        public Person(string firstName, string lastName) {
            this.firstName = firstName;
            this.lastName = lastName;
        }

        public string FirstName { get => firstName; }
        public string LastName { get => lastName; }

        public string GetFullName() {
            return firstName + " " + lastName;
        }
    }
}
