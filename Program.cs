﻿using System;
using System.Collections.Generic;

/**
 * Task 5 - Name Search
 *
 * Write a program which stores a sample of 5 contact names
 * - First and Last name
 * Then allows the user to search the list by name and determine if the name is in the list
 * - Partial matches should work
 * - Display all possible matches
 * 
 * 
 * 
 * BE AWARE, ASSUMPTIONS WAS MADE.
 * I interpreted that the task wants a partial match on both first, middle and last name.
 * 
 **/

namespace ExperisNoroffTask5 {
    class Program {

        static void Main(string[] args) {

            List<Person> people = CreatePeopleList();

            Console.WriteLine("Write your full name to check if it's in the list");
            string inputName = Console.ReadLine();

            #region Perfect Match

            // Check for a perfect match.
            Person perfectMatch = LookForPerfectMatch(people, inputName);
            if(perfectMatch == null) {
                Console.WriteLine("\nCould not find a perfect match");
            } else {
                Console.WriteLine("\nYour name is in the list, SWEET! ");
            }

            #endregion


            #region Partial Matches

            // Find partial matches
            List<Person> partialMatches = FindPartialMatches(people, inputName);

            if (partialMatches.Count > 0 && perfectMatch == null) {
                Console.WriteLine("These names are full or partial matches:\n");

                foreach (Person person in partialMatches) {
                    Console.WriteLine($"  -  {person.GetFullName()}");
                }
            }

            else if(perfectMatch == null) Console.WriteLine("No partial matches found either");

            #endregion

            AskUserAboutRestart();
        }



        /// <summary>
        ///     Iterates over the list of Person objects and check if the users name has an exact match.
        ///     Returns null if no exact match or the Person object if exact match is found.
        /// </summary>
        /// <param name="people">List of Person objects</param>
        /// <param name="inputName">The users name</param>
        /// <returns>The person object that is an exact match or null if no match is found</returns>
        static Person LookForPerfectMatch(List<Person> people, string inputName) {

            foreach (Person p in people) 
                if(p.GetFullName().ToLower().Equals(inputName.ToLower())) 
                    return p;
                
            return null;
        }



        /// <summary>
        ///     Takes a list of Person objects and a string representing the users inputted name.
        ///     Iterates over the list, and checks if each of the users names is contained in 
        ///     the full name of each of the Person objects in the list.    
        /// </summary>
        /// 
        /// <example>
        ///     userInput = "Odd Martin Hansen"
        ///     The method splits the name on space ["Odd", "Martin", "Hansen"]
        ///     It then checks if one of the three names in the array is contained in any of the Person
        ///     objects full names.
        /// </example>
        /// 
        /// <param name="people">List of Person objects</param>
        /// <param name="inputName">The users input string</param>
        /// <returns>A list of Person objects that contains parts of the users name.</returns>
        static List<Person> FindPartialMatches(List<Person> people, string inputName) {

            List<Person> matches = new List<Person>();

            string[] names = inputName.Split(" ");

            foreach (Person p in people) {
                foreach (string name in names) {
                    if(p.GetFullName().ToLower().Contains(name.ToLower())) {
                        if( !matches.Contains(p) ) matches.Add(p);
                        break;
                    }
                }
            }

            return matches;
        }



        /// <summary>
        ///     Adds five people to to the people list.
        /// </summary>
        static List<Person> CreatePeopleList() {

            List<Person> people = new List<Person>();

            people.Add(new Person("Odd Martin", "Hansen"));
            people.Add(new Person("Dean", "von Schultz"));
            people.Add(new Person("Azadeh", "Kiri"));
            people.Add(new Person("Greg", "Linkaster"));
            people.Add(new Person("Håkon", "Råen"));

            return people;
        }



        /// <summary>
        ///     Asks user to enter y for yes or n for no.
        ///     Reruns Main if yes, exits if no.
        /// </summary>
        static void AskUserAboutRestart() {
            string usersRetryChoice = "";
            do {
                if (usersRetryChoice != "") Console.WriteLine("Please write y for yes or n for no");

                Console.WriteLine("Do you want to go again? (y/n)");

                usersRetryChoice = Console.ReadLine();

            } while (!(usersRetryChoice.Equals("y") || usersRetryChoice.Equals("n")));

            if (usersRetryChoice == "y") Main(null);
            else Environment.Exit(0);
        }
    }
}
